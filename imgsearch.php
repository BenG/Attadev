<?php


function search_images($image){
	$img = imagecreatefromjpeg($image);
	$size = getimagesize($image, $info);

	
	$kepek = array();
	$not_in_image = true;
	
	for($i = 0; $i <= $size[0]-1; $i++){
		for($j = 0; $j <= $size[1]-1; $j++){
			if(not_white($img, $i, $j)){
				if(!empty($kepek)){
					foreach($kepek as $kep){
						if(($kep['start_x'] <= $i && $kep['end_x'] >= $i) && ($kep['start_y'] <= $j &&  $kep['end_y'] >= $j)){
							$not_in_image = false;
						}
					}
					if($not_in_image){
						/*
						echo '<br>start_x: '.$i;
						echo '<br>start_y: '.$j;
						*/
						$kepek = array_merge($kepek, array(img_found($img, $i, $j)));
					}
					$not_in_image = true;
				}
				else{
					/*
					echo '<br>start_x: '.$i;
					echo '<br>start_y: '.$j;
					*/
					$kepek = array_merge($kepek, array(img_found($img, $i, $j)));
				
					
				}
			}
		}
	}
	/*
	echo "<pre>";
	var_dump($kepek);
	echo "</pre>";
	*/
	$i = 0;
	foreach($kepek as $kep){
		image_cut($img, $kep["start_x"], $kep["start_y"], $kep["end_x"], $kep["end_y"], $i);
		$i++;
	}
	imagefilter($img, IMG_FILTER_COLORIZE, 255, 0, 0);
	foreach($kepek as $kep){
		
		$black = ImageColorAllocate($img, 0, 0, 0);
		ImageRectangle($img, $kep["start_x"], $kep["start_y"], $kep["end_x"], $kep["end_y"], $black);
		imageJpeg($img, 'filtered.jpg');
	}
}	
	function not_white($img, $x, $y){
		$rgb = imagecolorat($img, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		if(($r < 253) and ($g < 253) and ($b < 253)){
			return true;
		}
		else{
			return false;
		}
	}
	
	function white($img, $x, $y){
		$rgb = imagecolorat($img, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		if(($r >= 252) and ($g >= 252) and ($b >= 248)){
			return true;
		}
		else{
			return false;
		}
	}
	
	function img_found($img, $x, $y){
		$img_start_x = $x;
		$img_start_y = $y;
		$img_end_y = 1000;
		$flag_x = 1;
		$flag_y = 1;
		while($flag_x){
			while($flag_y){
				if(white($img, $x, $y)){
					$img_end_y = $y-1;
					if(white($img, $x+1, $img_end_y)){
						$img_end_x = $x;
						$img_end_y = $y-1;
						/*
						echo '<br>kep_end_x: '.$img_end_x;
						echo '<br>kep_end_y: '.$img_end_y;
						echo '<br>';
						echo '<br>';
						*/
						$flag_x = 0;
						$flag_y = 0;
						$kep = array("start_x" => $img_start_x, "start_y" => $img_start_y, "end_x" => $img_end_x, "end_y" => $img_end_y);
					}
					else{
						$flag_y = 0;
					}
				}
				if($y-1 == $img_end_y){
					break;
				}
				$y++;
				
				
			}
			$flag_y = 1;
			$x++;
			if($x == 1000){
				break;
			}
		}
		return $kep;
	}
	
	function image_cut($img, $start_x, $start_y, $end_x, $end_y, $i){
        $new_image = imagecreatetruecolor($end_x-$start_x, $end_y-$start_y);
		
		imagecopymerge($new_image, $img, 0, 0, $start_x, $start_y, $end_x, $end_y, 100);
		if (!file_exists('./extract')) {
			mkdir('./extract', 0777, true);
		}
		ImageJpeg($new_image, "./extract/" . $i. "_kep.jpg");
        ImageDestroy($new_image);
		
		
	}
	
	function get_all_images(){
		$files = glob("./extract/*.jpg");
		$kepek = array();
		foreach($files as $jpg){
			array_push($kepek, $jpg);
		}
		return $kepek;
	}
	
	
	function order($kepek){
		$tmp;
		for ($i = count($kepek)-1; 0 <= $i; $i--) {
			for ($j=0; $j < $i; $j++) {
				$size = getimagesize($kepek[$j], $info);
				$size2 = getimagesize($kepek[$j+1], $info);
				if ($size[0]<$size2[0]) {
					$tmp=$kepek[$j];
					$kepek[$j]=$kepek[$j+1];
					$kepek[$j+1]=$tmp;
				}
			}
		}
		return $kepek;
	}
	
?>
















